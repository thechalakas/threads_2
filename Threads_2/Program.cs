﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Threads_2
{
    

    class Program
    {
        public static int sleep_duration = 1000;

        static void thread_demo()
        {
            for(int i=0;i<5;i++)
            {
                Console.WriteLine("thread_demo - value of i is {0}", i);
                //making the thread to go sleep
                Thread.Sleep(sleep_duration);
            }
        }

        static void Main(string[] args)
        {
            //create a new thread with the above demo method
            Thread t = new Thread(new ThreadStart(thread_demo));
            Console.WriteLine("Enter b for background anything else for foreground");
            string temp = Console.ReadLine();

            //here, if you enter b - when the program runs - the app will quit right away
            //thats because background threads are terminated when application reaches its end
            if (temp.Equals("b"))
            {
                //when you enter b, we will set the thread as background
                //when you set a thread as background then the program waits till the 
                //other threads - like this main thread which are foreground by default - to finish
                t.IsBackground = true;
            }

            t.Start();

        }
    }
}
